package config

import (
	"database/sql"
	"fmt"
	"log"
	"os"

	_ "github.com/go-sql-driver/mysql"
	"github.com/golang-migrate/migrate"
	"github.com/golang-migrate/migrate/database/mysql"
	_ "github.com/golang-migrate/migrate/source/file"
)

func migrates() *migrate.Migrate {
	db, err := sql.Open("mysql", fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?multiStatements=true",
		os.Getenv("DB_USERNAME"),
		os.Getenv("DB_PASSWORD"),
		os.Getenv("DB_HOST"),
		os.Getenv("DB_PORT"), os.Getenv("DB_DATABASE")))

	if err != nil {
		panic(err)
	}
	driver, _ := mysql.WithInstance(db, &mysql.Config{})
	m, err := migrate.NewWithDatabaseInstance(
		"file://migrations/",
		"mysql",
		driver,
	)

	if err != nil {
		panic(err)
	}

	return m
}

func MigrateUp() {

	defer migrationPanicHandler()

	migrator := migrates()
	if err := migrator.Up(); err != nil {
		panic(err)
	}
}

func MigrateDown() {

	defer migrationPanicHandler()

	migrator := migrates()
	if err := migrator.Down(); err != nil {
		panic(err)
	}
}

func migrationPanicHandler() {
	
	if r := recover(); r != nil {
		log.Println("Migration ", fmt.Sprintf("%v", r))
	}
}