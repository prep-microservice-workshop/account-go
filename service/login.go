package service

import (
	"myapp/config"
	"myapp/graph/model"
	"myapp/tools"

	"github.com/vektah/gqlparser/v2/gqlerror"
	"gorm.io/gorm"
)

func Login(input model.NewUser) (string, error) {

	db := config.GetDB()

	var user model.User
	err := db.Model(&model.User{}).Where("username = ?", input.Username).Take(&user).Error
	if err != nil {
		if err == gorm.ErrRecordNotFound {
			return "", gqlerror.Errorf("User not found , Please proceed to registration!")
		}else {
			panic(err)
		}
	}

	if !tools.CheckHash(user.HashedPassword, input.Password) {
		return "", gqlerror.Errorf("Incorrect Password!")
	}

	token, err := config.TokenGenerate(user.ID, user.Username)
	if err != nil {
		panic(err)
	}

	return token, nil
}