package service

import (
	"context"
	"myapp/config"
	"myapp/graph/model"

	"github.com/vektah/gqlparser/v2/gqlerror"
	"gitlab.com/prep-microservice-workshop/utils-go.git/middleware"
	"gorm.io/gorm"
)

func MeGet(ctx context.Context) (*model.User, error) {

	loggedInUser := middleware.AuthContext(ctx)
	if loggedInUser == nil {
		return nil, gqlerror.Errorf("You are not logged In!")
	}

	db := config.GetDB()

	var user model.User
	err := db.Model(&model.User{}).
		Where("id = ? AND username = ?", loggedInUser.ID, loggedInUser.Username).Take(&user).Error

	if err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, gqlerror.Errorf("User not found, Invalid token!")
		} else {
			panic(err)
		}
	}

	return &user, nil
}

func UsersGetByWhereInIDs(ids []int) []*model.User {

	db := config.GetDB()

	var users []*model.User
	err := db.Model(&model.User{}).Where("id in (?)", ids).Find(&users).Error
	if err != nil {
		panic(err)
	}

	return users
}

func UserLoader(keys []int) ([]*model.User, []error) {

	users := UsersGetByWhereInIDs(keys)
	userMappedByID := map[int]*model.User{}

	for _, user := range users {
		userMappedByID[user.ID] = user
	}

	result := make([]*model.User, len(keys))
	for i, id := range keys {
		result[i] = userMappedByID[id]
	}

	return result, nil
}

func UserGetByID(id int) (*model.User, error) {

	db := config.GetDB()

	var user model.User
	err := db.Model(&model.User{}).
		Where("id = ?", id).Take(&user).Error

	if err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, gqlerror.Errorf("User not found, Invalid token!")
		} else {
			panic(err)
		}
	}

	return &user, nil
}
